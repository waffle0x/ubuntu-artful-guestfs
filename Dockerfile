FROM ubuntu:artful
LABEL maintainer="neta540@gmail.com"

WORKDIR /root

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install --no-install-recommends -y \
  libguestfs-tools \
  qemu-utils \
  linux-image-generic \
  binfmt-support \
  qemu \
  qemu-user-static
	

ENV LIBGUESTFS_BACKEND=direct